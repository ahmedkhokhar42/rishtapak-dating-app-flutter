# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
** Flutter Complete Dating App for Android & iOS with Admin Panel is a full functional application that is ready to go production, however you need to follow some Steps to build your app release and publish it on your Google Play Store or Apple Store account.
### App Features
*VIP Subscriptions - (In-App purchases)
*Admob Interstials Ads
*Multi-language support
*Push notifications - (New like, visit and message)
*Swipe Left/Right - (like tinder profile cards)
*It's a Match dialog
*Chat with text and image
*Sign in with phone number
*Report User Profile feature
*Profile statistics (Total likes, visits, dislikes)
*Share the app with friends
*Rate app on app stores
*Get location by GPS
*Show Users based on geolocation distance radius in (km)
*Update location
*Change max distance radius in (km)
*Change age range filter
*Alert user to enable GPS
*Redirect user to enable location permission in device settings
*Redirect user to blocked account screen
*Redirect user to update app
*Sign out button
*Delete Account button
*Hide user profile
*Show me Filter: Men, Women or Everyone
*Passport feature - Latest update
*Backend with Cloud Firestore
*Firebase Free Account Support

### App Screens
*Splash Screen
*Sign In Screen
*Login with Phone Number Screen
*Verification Code Screen (OTP)
*Sign Up Screen
*Home Screen
*Profile Screen
*Profile Likes Screen
*Profile Visits Screen
*Disliked Profiles Screen
*Edit Profile Screen
*Notifications Screen
*Chat Screen
*It’s Match Dialog
*About Us Screen
*Blocked Account Screen
*Enable Location Screen
*Settings Screen
*Update App Screen
*Delete Account Screen
*Passport Screen

### Tabs

*Discover Tab
*Matches Tab
*Conversations Tab
*Profile Tab

### Icons

*Beautiful outline SVG icons used in project

### 02) Project requirements -

*Flutter SDK Version: 2.0.6 or later - Null Safety Support
*Dart Version: 2.12.0 or later - Null Safety Support
*Android target SDK version: 30 or later
*Recommended Editors: Visual Studio Code/ Android Studio/ or Xcode for iOS


### Get Started with Flutter - top
What is flutter?

Flutter is an open-source cross-platform mobile application SDK developed by Google.
It is used to develop applications for Android, iOS, Linux, Mac, Windows, Google Fuchsia, and the web from a single codebase. Flutter is written in Dart language.

Why Flutter?

Flutter saves companies the need to employ different iOS and Android developers to build the same app since it supports cross-platforms development.

Development Platforms

After having a basic idea of what flutter is, now it's time to setup your dating_app project.

Flutter requires Flutter SDK to be installed first.
Please check this link https://flutter.dev/docs/get-started/install for official documentation on steps how to install Flutter SDK on your Operating System and set up your favourable editor.

### 04) Open Dating App Project - top
How to open project using the Visual Studio Code; - (For other IDEs please follow their official instructions)

Start the Visual Studio Code. or your favorite IDE Editor
Select File > Open Folder from the main menu and then choose > Flutter Dating App > SOURCE CODES > dating_app folder.
Run the this command: flutter pub get on your Terminal Editor to get flutter packages.